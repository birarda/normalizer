# normalizer.rb

This ruby script takes a CSV file on `stdin` and outputs a normalized version of that file to `stdout`.

## Usage

`cat sample.csv | ruby normalizer.rb`

## Requirements

You should not require anything but Ruby - no additional gems are required. 

Ruby 2.4 added improved Unicode support. If your version of Ruby is not 2.4 or later, the upcasing of characters during normalization will not upcase some non-English letters. 
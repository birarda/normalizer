require 'time'

class RowData
  def initialize(line)
    # set our values from the results of split_line
    @timestamp, @address, @zip, @name,
    @foo_duration, @bar_duration, @total_duration, @notes = split_line(line)

    # start with the assumption that this row is valid
    @valid_row = true
  end

  def valid_row?
    @valid_row
  end

  def to_s
    # make sure parsing didn't fail on something in this row
    if @valid_row
      [
        @normalized_timestamp, @address, @normalized_zip, @normalized_name,
        @normalized_foo_duration, @normalized_bar_duration,
        (@normalized_foo_duration + @normalized_bar_duration).round(3),
        @notes
      ].join(',')
    else
      ""
    end
  end

  def normalize
    # call all of our normalizers to generate our normalized member variables
    normalize_timestamp
    normalize_zip
    normalize_name
    normalize_foo_duration
    normalize_bar_duration
  end

  def normalize_timestamp
    begin
      # parse a time object from the string
      pacific_time = Time.strptime @timestamp, "%m/%e/%y %l:%M:%S %p"

      # assume the parsed times were actually in US/Pacific
      # and offset them to US/Eastern
      eastern_time = pacific_time + 3 * 3600

      # store an ISO-8601 string of the adjusted time
      @normalized_timestamp = eastern_time.strftime "%FT%T"

    rescue StandardError => e
      # with more time it would be better to catch specific exceptions and
      # warn with a message specific to the given exception

      warn "Could not parse timestamp in expected format. Excluding row from output."

      # flag the row as invalid so it will not be output
      @valid_row = false
    end
  end

  def normalize_zip
    @normalized_zip = @zip.rjust(5, "0")
  end

  def normalize_name
    # this is simple since ruby has upcase, but I could have
    # done an enumeration through the string and performed replacements
    # with the table from here
    # https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_72/nls/rbagslowtoupmaptable.htm

    # note that for upcase to handle the full unicode case mapping
    # for most languages ruby 2.4 and above is necessary
    @normalized_name = @name.upcase
  end

  def normalize_foo_duration
    @normalized_foo_duration = normalized_duration(@foo_duration)
  end

  def normalize_bar_duration
    @normalized_bar_duration = normalized_duration(@bar_duration)
  end

  private

  def normalized_duration(string_duration)

    # use a regex to parse out the hours, minutes, seconds, and milliseconds
    result = string_duration.match(/(\d+):(\d+):(\d+).(\d+)/)

    # make sure that the regex captured four elements
    if result && result.captures.length == 4
      hours, minutes, seconds, milliseconds = result.captures.map(&:to_i)
      return (hours * 3600) + (minutes * 60) + seconds + (milliseconds / 1000.0)
    else
      warn "Could not parse duration in expected HH:MM:SS.MS format. Excluding row from output."

      # flag the row as invalid so it will not be output
      @valid_row = false
    end
  end

  QUOTE_PAIRS = {
    "\u0022" => "\u0022", # quotation mark
    "\u0027" => "\u0027", # apostrophe
    "\u2018" => "\u2019", # left/right single quotation mark
    "\u201C" => "\u201D" # left/right double quotation mark
  }

  def split_line(line)
    # prepare an array for values to return at the end
    values = []

    # start the offset for search at 0
    offset = 0

    while offset < line.length do
      # first check if this value is quoted
      first_char = line[offset]

      if QUOTE_PAIRS.key?(first_char)
        # find out where the quoted string ends and pull out the values
        closed_quote_index = line.index QUOTE_PAIRS[first_char], offset + 1

        # grab the value from the opening quote inclusive
        # to the closing quote inclusive
        values << line[offset..closed_quote_index]

        offset = closed_quote_index + 2
      else
        # not a quoted value, assume it ends right before the next comma
        after_value_index = line.index ',', offset

        # if this is the last value set the end index past the end of the line
        after_value_index = line.length if !after_value_index

        # append the value grabbing a substring
        # from the current offset inclusive to next comma exclusive
        values << line[offset...after_value_index]

        # set the new search index to start where the next value does
        offset = after_value_index + 1
      end
    end

    # return the constructed array of values
    return values
  end
end

# check if the user might need help figuring out how to call the tool
if !ARGV.empty?
  puts "normalizer.rb - A CSV Normalizer for Truss application work sample"
  puts "==================="
  puts "Pass a CSV formatted file on stdin. A normalized CSV formatted file will be emitted on stdout."
else
  # otherwise assume that we are reading CSV from stdin

  # note that ruby has a class to interface with CSV files
  # I assumed that using that would be against the spirit of the assignment

  # grab the piped CSV and scrub any invalid characters
  # replacing them with U+FFFD (the unicode replacement character)
  input = ARGF.read.scrub "\ufffd"

  # split the input on line breaks
  input_lines = input.split "\n"

  # enumerate the lines in the piped CSV
  input_lines.each_with_index do |line, index|

    if index == 0
      # this is the header which gets copied over as is
      puts line
    else
      # construct a new RowData from the line, splitting out the values
      row = RowData.new line

      # normalize the data in the row
      row.normalize

      # output the normalized row if it was succesfully parsed and normalized
      puts row if row.valid_row?
    end
  end
end
